let mainHeader = document.querySelector('.main-header');
let burger = document.querySelector('.toggle-button');
let closeButton = document.querySelector('.closing-button');
let navMobile = document.querySelector ('.mobile-nav');
let activeClass = document.querySelector('.active-nav');
let activeClassMain = document.querySelector('.active-nav__main');
let toggle = document.querySelector('.toggle');

mainHeader.addEventListener('click',(el)=>{
    if(el.target.closest('.mobile-nav__item')){
        const li =  el.target.closest('.mobile-nav__item');
        li.firstElementChild.classList.toggle("active-nav");
        activeClass.classList.toggle("active-nav");
        activeClass = li.firstElementChild;
    }else if(el.target.closest('.main-nav__item')){
        const liMain =  el.target.closest('.main-nav__item');
        liMain.firstElementChild.classList.toggle("active-nav__main");
        activeClassMain.classList.toggle("active-nav__main");
        activeClassMain = liMain.firstElementChild;
    }
});

//toggle має position absolute;

toggle.addEventListener('click',(el)=>{
    if (el.target.closest('.toggle-button')){
        burger.style.display = 'none';
        closeButton.style.display = 'block';
        navMobile.classList.toggle('active');

    } else if(el.target.closest('.closing-button')) {
        closeButton.style.display = 'none';
        burger.style.display = 'block';
        navMobile.classList.toggle('active');
    }
})